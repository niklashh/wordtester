import React, {Component} from 'react';

class App extends Component {
  render() {
    return <h1>Welcome</h1>;
  }
}

export default App;

// vim: et ts=2 sw=2 :
