const mongoose = require('mongoose');

const classSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
});
classSchema.add({
  children: [classSchema],
});

module.exports = mongoose.model('Class', classSchema);

// vim: et sw=2 ts=2 :
