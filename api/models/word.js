const mongoose = require('mongoose');

const wordSchema = mongoose.Schema({
  translations: {
    type: Object,
    default: {},
  },
});

module.exports = mongoose.model('Word', wordSchema);

// vim: et sw=2 ts=2 :
