const words = require('./words');
const classes = require('./classes');

module.exports = {
  routeWords: words,
  routeClasses: classes,
};

// vim: et ts=2 sw=2 :
