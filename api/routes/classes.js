const express = require('express');
const mongoose = require('mongoose');

const Class = require('../models/class');
const auth = require('../auth');

const router = express.Router();

router.get('/', (req, res) =>
  Class.find().then(words => res.status(200).json(words)),
);

router.post('/', (req, res) =>
  new Class(req.body).save().then(result => res.status(201).json(result)),
);

router.patch('/', (req, res) =>
  Class.update({_id: req.body._id}, req.body).then(result =>
    res.status(200).json(result),
  ),
);

/*
 * Patch req.body.parentId moving the Class
 * matching req.body.childrenId
 * to its children property
 */
router.patch('/extend', (req, res) =>
  Class.findOne({_id: req.body.parentId})
    .then(parentdoc => {
      Class.findOne({_id: req.body.childrenId}).then(childdoc => {
        parentdoc.children.push(childdoc);
        parentdoc.save();
        // At this point we have two documents with
        // the same _id
        // TODO Figure out the correct way
        childdoc.remove().then(result => res.status(200).json(result));
      });
    })
    .catch(error => res.status(400).json({error})),
);

module.exports = router;

// vim: et ts=2 sw=2 :
