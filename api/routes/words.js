const express = require('express');
const mongoose = require('mongoose');

const Word = require('../models/word');
const auth = require('../auth');

const router = express.Router();

router.get('/', (req, res) =>
  Word.find().then(words => res.status(200).json(words)),
);

router.post('/', (req, res) =>
  new Word({translations: req.body})
    .save()
    .then(result => res.status(201).json(result)),
);

router.patch('/', (req, res) =>
  Word.update({_id: req.body._id}, req.body).then(result =>
    res.status(200).json(result),
  ),
);

module.exports = router;

// vim: et ts=2 sw=2 :
